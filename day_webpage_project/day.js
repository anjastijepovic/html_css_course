

// PULL POSTS FROM DB

async function updatePostsContainer() {
  const container = document.getElementById('posts-container');

  const response = await fetch('http://127.0.0.1:5000/posts');
  const posts = await response.json();
  
  container.innerHTML = '';
  
  posts.forEach(post => {
    const postElement = document.createElement('div');
    postElement.classList.add('post');
    

    const imageElement = document.createElement('img');
    imageElement.src = post[0]; 
    imageElement.alt = 'Post Image';
    postElement.appendChild(imageElement);
    

    const titleElement = document.createElement('h2');
    titleElement.textContent = post[1]; 
    postElement.appendChild(titleElement);
    
    // const descriptionElement = document.createElement('p');
    // descriptionElement.textContent = post[2]; 
    // postElement.appendChild(descriptionElement);
    
    const categoryElement = document.createElement('p');
    categoryElement.textContent = post[3]; 
    postElement.appendChild(categoryElement);
    
    container.appendChild(postElement);
  });
}

updatePostsContainer()


// Filter posts based on categories

const categoryButtons = document.querySelectorAll('.category-btn');


categoryButtons.forEach(button => {
  button.addEventListener('click', () => {
    const category = button.textContent.toUpperCase(); 
    
      // filter posts
    const posts = document.querySelectorAll('.post');
    posts.forEach(post => {
      if (post.querySelector('p').textContent === category || category === 'ALL') {
        post.style.display = 'block'; 
      } else {
        post.style.display = 'none'; 
      }
    });
  });
});



// SEND MESSAGE TO THE DB

async function postJSON(data) {
    try {
        const response = await fetch("http://127.0.0.1:5000/messages", {
            method: "POST", 
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        });
      
        const result = await response.json();
        console.log(result);
        } catch (error) {
          console.error("Error:", error);
        }
    }
      
// const data = {"name": "Anja", "email": "anja@gmail.com", "subject": "Sales Rep", "message": "bla bla"};
// postJSON(data);

  const messageForm = document.getElementById('message-form');
  
  messageForm.addEventListener('submit', onSubmit);
      
  function onSubmit(event){
    event.preventDefault();
      
    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    const subject = document.getElementById('subject').value;
    const message = document.getElementById('message').value;
    const msg = document.querySelector('.msg');
    
    if (name && email && subject && message) {
        const data = {
            name: name,
            email: email,
            subject: subject,
            message: message
        };
        
        postJSON(data);
        msg.innerHTML = 'You successfully submitted a message';
        setTimeout(() => msg.remove(), 3000);
        // da se makne poruka poslije 3 sekunde
    } else {
        console.error("Please fill in all fields.");
    }
  };

 
