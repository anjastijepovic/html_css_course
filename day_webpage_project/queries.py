from db_connection import cursor, db
from mysql.connector import Error

def get_posts():
    sql = ("SELECT imageUrl, title, imageDescription, category FROM posts")
    cursor.execute(sql)
    result = cursor.fetchall()
    return result
    
# print(get_posts())

def send_message(name, email, subject, message):
    sql = ("INSERT INTO messages(name, email, subject, message) VALUES (%s, %s, %s, %s)")
    try:
        cursor.execute(sql, (name, email, subject, message,))
        db.commit()
        return "Successfully added a message." 
    except Error as error:
        return error.msg, 404
    
