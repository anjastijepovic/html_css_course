from flask import Flask, request, jsonify
from flask_restful import Api, Resource, reqparse
from flask_cors import CORS
from queries import *

app = Flask(__name__)

CORS(app) 

api = Api(app)

posts_args = reqparse.RequestParser()

messages_args = reqparse.RequestParser()

messages_args.add_argument("name", type = str, required = True)
messages_args.add_argument("email", type = str, required = True)
messages_args.add_argument("subject", type = str, required = True)
messages_args.add_argument("email", type = str, required = True)
messages_args.add_argument("message", type = str, required = True)



class Posts(Resource):

    def get(self):
        return get_posts()
    

class Messages(Resource):

    def post(self):
        args = messages_args.parse_args()
        result = send_message(
        args['name'],
        args['email'],
        args['subject'],
        args['message']
        )
        return result

api.add_resource(Posts, "/posts") 
api.add_resource(Messages, "/messages") 


if __name__ == "__main__":
    app.run(debug=True)