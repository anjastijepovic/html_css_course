CREATE DATABASE IF NOT EXISTS day_webpage;

USE day_webpage;

CREATE TABLE IF NOT EXISTS messages (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    email VARCHAR(255) NOT NULL,
    subject VARCHAR(100)  NOT NULL,
    message VARCHAR(1000) NOT NULL
);

-- INSERT INTO posts (imageUrl, title, imageDescription, category) 
-- VALUES ('img/portfolio/portfolio-4.jpg', 'Card 2', 'Bla bla', 'CARD');

-- INSERT INTO posts (imageUrl, title, imageDescription, category) 
-- VALUES ('img/portfolio/portfolio-5.jpg', 'Web 2', 'Bla bla', 'WEB');

-- INSERT INTO posts (imageUrl, title, imageDescription, category) 
-- VALUES ('img/portfolio/portfolio-6.jpg', 'App 3', 'Bla bla', 'APP');

-- INSERT INTO posts (imageUrl, title, imageDescription, category) 
-- VALUES ('img/portfolio/portfolio-7.jpg', 'Card 1', 'Bla bla', 'CARD');

-- INSERT INTO posts (imageUrl, title, imageDescription, category) 
-- VALUES ('img/portfolio/portfolio-8.jpg', 'Card 3', 'Bla bla', 'CARD');

-- INSERT INTO posts (imageUrl, title, imageDescription, category) 
-- VALUES ('img/portfolio/portfolio-9.jpg', 'Web 3', 'Bla bla', 'WEB');


